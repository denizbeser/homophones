import gensim
import logging
import time
import random
from nltk.corpus import wordnet as wn

import numpy as np
import matplotlib.pyplot as plt

# Logging code taken from http://rare-technologies.com/word2vec-tutorial/
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

start = time.time()
model = gensim.models.KeyedVectors.load_word2vec_format('./GoogleNews-vectors-negative300.bin', binary=True)  
end = time.time()
print 'Loaded model in ', end-start
nouns = {x.name().split('.', 1)[0] for x in wn.all_synsets('n')}

def get_homo_pairs(path):
	file = open(path, 'r')
	pairs = []
	for line in file:
		words = line.replace(',','').split()
		if len(words) == 2:
			pairs.append((words[0],words[1]))
	return pairs

def get_random_google_pairs(n):	
	words = model.wv.vocab.keys()
	l = len(words)-1
	pairs = []
	while len(pairs) < n:
		w1,w2 = (words[random.randint(0,l)], words[random.randint(0,l)])
		if w1 in nouns and w2 in nouns:
			pairs.append((w1,w2))
	return pairs	

def calc_average_dist(pairs, title):
	#find average pair distance
	count = 0
	total = 0.0
	similarities = []
	for w1, w2 in pairs:
		if w1 in model.wv.vocab and w2 in model.wv.vocab:
			if w1 in nouns and w2 in nouns:
				similarity = model.wv.similarity(w1, w2)
				#similarity = similarity if similarity > 0 else 0
				total = total + similarity
				count = count + 1		
				similarities.append(similarity)
	print 'calculated dist with ', count, ' words'

	fig = plt.figure()
	ax = fig.add_subplot(111)

	x = similarities
	numBins = 50
	ax.hist(x, numBins, color='green',alpha=0.8)
	ax.set_xlim(xmin=-0.2, xmax=1.0)
	ax.set_xlabel('Similarity')
	ax.set_ylabel('Frequency')
	ax.set_title(title)
	plt.savefig(title.lower()+'.png')
	return total/count

homo_path = 'homophones.txt'
homo_pairs = get_homo_pairs(homo_path)
print 'Average Homophone Distance:', calc_average_dist(homo_pairs, 'W2V_Homophones')

random_pairs = get_random_google_pairs(147)
print 'Average Google News Distance:', calc_average_dist(random_pairs, 'W2V_Random_Google')

# print 'Bone: ', model.wv.similarity('dog', 'bone')
# print 'Cat:',model.wv.similarity('dog', 'cat')
# print 'Bat:',model.wv.similarity('dog', 'bat')
# print 'Animal:', model.wv.similarity('dog', 'animal')
# print 'Chair:', model.wv.similarity('dog', 'chair')



