from gensim.models.wrappers import FastText
import logging
import time
import random

import numpy as np
import matplotlib.pyplot as plt

# Logging code taken from http://rare-technologies.com/word2vec-tutorial/
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

start = time.time()
model = FastText.load_fasttext_format('./zh/zh.bin')
end = time.time()
print 'Loaded model in ', end-start

def get_eng_nouns(path):
	file = open(path, 'r')
	nouns = []
	for line in file:
		line_data = line.split()
		word = line_data[0]
		pos = line_data[1]
		if pos == 'NN':
			nouns.append(word.lower())
	print len(nouns), 'english nouns'
	return nouns

def get_chinese_nouns(path, eng_nouns):
	file = open(path, 'r')
	c_nouns = set([])
	for line in file:
		line_data = line.split()
		if len(line_data) > 5:
			char = line_data[1].decode('utf-8')
			eng_data = line_data[5].split('/')
			eng_word = eng_data[0] if ' ' not in eng_data[0] else None
			if eng_word.lower() in eng_nouns:
				c_nouns.add(char)
	print len(c_nouns), 'chinese nouns'
	return c_nouns

def get_homo_pairs(path, c_nouns):
	words = model.wv.vocab.keys()
	morp_to_word = {}
	homo_path = path
	file = open(homo_path, 'r')
	char_to_morp_map = {}
	for line in file:
		line_data = line.split()
		if len(line_data) > 4:
			char = line_data[1].decode('utf-8')
			morp = line_data[4].split('/')[0]
			char_to_morp_map[char] = morp

	#create map of pinyin to word
	for word in words:
		morps = []
		broken = False
		for c in list(word):
			if c not in char_to_morp_map or c not in c_nouns: 
				broken = True
				break
			morps.append(char_to_morp_map[c])
		if broken: continue

		pinyin = ''.join(morps)
		#print 'PINYIN:', pinyin
		if pinyin in morp_to_word:
			morp_to_word[pinyin].append(word)
		else:
			morp_to_word[pinyin] = [word]

	#filter out homophones
	homo_map = {m:l for m,l in morp_to_word.iteritems() if len(l) > 1}
	# for m,l in homo_map.iteritems():
	# 	print 'Morp:', m
	# 	for c in l: print c
	pairs = set([])
	for l in homo_map.values():
		count = 0
		for x in xrange(len(l)):
			for y in xrange(x+1,len(l)):
				count = count + 1
				pairs.add((l[x],l[y]))
	return pairs

def get_random_chinese_pairs(n, nouns):	
	words = model.wv.vocab.keys()
	l = len(words)-1
	pairs = []
	while len(pairs) < n :
		w1,w2 = (words[random.randint(0,l)], words[random.randint(0,l)])
		if is_noun(w1, nouns) and is_noun(w2, nouns):
			pairs.append((w1,w2))
	return pairs

def is_noun(word, nouns):
	for c in list(word):
		if c not in nouns: 
			return False
	return True


def calc_average_dist(pairs, title):
	#find average pair distance
	count = 0
	total = 0.0
	similarities = []
	for w1, w2 in pairs:
		if w1 in model.wv.vocab and w2 in model.wv.vocab:
		#if w1 in nouns and w2 in nouns:
			similarity = model.wv.similarity(w1, w2)
			#similarity = similarity if similarity > 0 else 0
			total = total + similarity
			count = count + 1
			similarities.append(similarity)
		else:
			print 'Not in:',w1, w2
	print 'calculated dist with ', count, ' words'

	fig = plt.figure()
	ax = fig.add_subplot(111)

	x = similarities
	numBins = 50
	ax.hist(x, numBins, color='green',alpha=0.8)
	ax.set_xlim(xmin=-0.2, xmax=1.0)
	ax.set_xlabel('Similarity')
	ax.set_ylabel('Frequency')
	ax.set_title(title)
	plt.savefig(title.lower()+'.png')
	return total/count

eng_nouns = get_eng_nouns('celex-dict_english.txt')
c_nouns = get_chinese_nouns('chinese-words.txt', eng_nouns)
homo_pairs = get_homo_pairs('chinese-words.txt', c_nouns)
print len(homo_pairs), 'homo pairs'
random_pairs = get_random_chinese_pairs(len(homo_pairs), c_nouns)

print 'Average Homophone Distance:', calc_average_dist(homo_pairs, 'W2V_Homophones')
print 'Average Chinese Distance:', calc_average_dist(random_pairs, 'W2V_Random_Chinese')

# print 'Bone: ', model.wv.similarity('dog', 'bone')
# print 'Cat:',model.wv.similarity('dog', 'cat')
# print 'Bat:',model.wv.similarity('dog', 'bat')
# print 'Animal:', model.wv.similarity('dog', 'animal')
# print 'Chair:', model.wv.similarity('dog', 'chair')



