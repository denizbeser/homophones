from nltk.corpus import wordnet as wn
import os
import string
import random

import numpy as np
import matplotlib.pyplot as plt

def get_homo_pairs(path):
	file = open(path, 'r')
	pairs = []
	for line in file:
		words = line.replace(',','').split()
		if len(words) == 2:
			pairs.append((words[0],words[1]))
	return pairs

def get_random_brown_pairs(path, n):
	file = open(path, 'r')
	words = [word.split('=')[0].lower() for line in file for word in line.strip(string.punctuation).split()]
	nouns = {x.name().split('.', 1)[0] for x in wn.all_synsets('n')}
	# pairs = [(word, words[random.randint(0,len(words)-1)]) for word in words]		
	pairs = []
	while len(pairs) < n:
		w1,w2 = (words[random.randint(0,len(words)-1)], words[random.randint(0,len(words)-1)])
		if w1 in nouns and w2 in nouns:
			pairs.append((w1,w2))
	return pairs

def calc_average_dist(pairs, title):
	#find average homonym distance
	count = 0
	total = 0.0
	similarities = []
	for w1, w2 in pairs:
		try: synset1 = wn.synset(w1+'.n.01')
		except:
			continue
			# try: synset1 = wn.synset(w1+'.v.01')
			# except: continue
		try: synset2 = wn.synset(w2+'.n.01')
		except:
			continue
			# try: synset2 = wn.synset(w2+'.v.01')
			# except: continue		
		similarity = synset1.wup_similarity(synset2)
		similarity = similarity if similarity > 0 else 0
		similarities.append(similarity)
		total = total + similarity
		count = count + 1		
	print 'Distance from ', count, ' pairs'

	fig = plt.figure()
	ax = fig.add_subplot(111)

	x = similarities
	numBins = 50
	ax.hist(x,numBins,color='green',alpha=0.8)
	ax.set_xlabel('Similarity')
	ax.set_ylabel('Frequency')
	ax.set_title(title)
	plt.savefig(title.lower()+'.png')
	return total/count

homo_path = 'homophones.txt'
homo_pairs = get_homo_pairs(homo_path)
print 'Average Homophone Distance:', calc_average_dist(homo_pairs, 'Wordnet_Homophones')

brown_path = 'brown_corpus.txt'
random_pairs = get_random_brown_pairs(brown_path, 227)
print 'Average Brown Distance:', calc_average_dist(random_pairs, 'Wordnet_Random_Brown')

synset1 = wn.synset('dog.n.01')
try: 
	synset2 = wn.synset('dalmatian.n.01')
	print 'Dalmatian:',synset1.wup_similarity(synset2)
except: pass

# synset1 = wn.synset('dog.n.01')
# synset2 = wn.synset('bone.n.01')
# print 'Bone:',synset1.wup_similarity(synset2)
# synset1 = wn.synset('dog.n.01')
# synset2 = wn.synset('cat.n.01')
# print 'Cat:',synset1.wup_similarity(synset2)
# synset1 = wn.synset('dog.n.01')
# synset2 = wn.synset('bat.n.01')
# print 'Bat:',synset1.wup_similarity(synset2)
# synset1 = wn.synset('dog.n.01')
# synset2 = wn.synset('animal.n.01')
# print 'Animal:', synset1.wup_similarity(synset2)
# synset1 = wn.synset('dog.n.01')
# synset2 = wn.synset('chair.n.01')
# print 'Chair:', synset1.wup_similarity(synset2)



